// berisi untuk menampilan detail event dari yang di pilih
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:eventnextgen/Event.dart';

class DetailEventScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Detail Event',
          style: TextStyle(color: white),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          color: (Colors.black),
          onPressed: () => {
            Navigator.push(context, MaterialPageRoute(builder: (context) => EventScreen()))
          },
        ),
        backgroundColor: Colors.red,
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
              Container(
                height: 100,
                width: 300,
                color: Colors.grey,
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 15, bottom: 15),
                6.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 20,
                      ),
                    ),
                  ],
                ).paddingOnly(top: 4),
                10.height,
                const Text(
                'STEMPRENEUR CAMP: DIGITAL MARKETING 101 - DAY 2',
                style: TextStyle(fontSize: 14),
                ),
                12.height,
                const Text(
                'Buat Bisnis Laris Tanpa Meringis? Belajar Digital Marketing Dong!',
                style: TextStyle(fontSize: 14),
                ),
                14.height,
                const Text(
                'STEMPreneur Center sebagai inkubator bisnis berbasis STEM akan mengadakan pelatihan khusus secara GRATIS untuk SELURUH PRASMULYAN',
                style: TextStyle(fontSize: 14),
                ),
                18.height,
                const Text(
                'Pelatihan ini secara ekslusif dibawakan oleh Izzahtul Mujahidah, mahasiswa Digital Business Technologi yang juga merupakan PPC Specialist at Educourse.id',
                style: TextStyle(fontSize: 14),
                ),
                20.height,
                const Text(
                'IMPORTANT DATES: Jumat, 4 Maret 2022 pukul 16.00 WIB',
                style: TextStyle(fontSize: 14),
                ),
                24.height,
                const Text(
                'REGISTRATION: https://bit.ly/StempreneurCamp',
                style: TextStyle(fontSize: 14),
                ),
                26.height,
                const Text(
                'Tunggu apa lagi? Dongkrak penjualanmu dengan digital marketing! #STEMPreneur #STEMPrasmul #STEMPreneurCamp',
                style: TextStyle(fontSize: 14),
                ),
                30.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Register'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
        ],
      ),
      )],    
    )]),
    )),
  );
  }
}