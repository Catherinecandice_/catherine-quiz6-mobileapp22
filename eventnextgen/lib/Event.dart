// berisi untuk menampilkan list event yang berasal dari API
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:nb_utils/nb_utils.dart';
import 'package:eventnextgen/DetailEvent.dart';

class EventScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: ListView(
        scrollDirection: Axis.vertical, 
        children: <Widget>[
        Card(
          shadowColor: Colors.grey.withOpacity(.4),
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(2))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 8),
                3.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                6.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Read More'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
              ],
            ).paddingOnly(top: 8),
          )
        ),
        Card(
          shadowColor: Colors.grey.withOpacity(.4),
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(2))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 8),
                3.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                6.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Read More'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
              ],
            ).paddingOnly(top: 8),
          )
        ),
        Card(
          shadowColor: Colors.grey.withOpacity(.4),
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(2))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 8),
                3.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                6.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Read More'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
              ],
            ).paddingOnly(top: 8),
          )
        ),
        Card(
          shadowColor: Colors.grey.withOpacity(.4),
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(2))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 8),
                3.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                6.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Read More'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
              ],
            ).paddingOnly(top: 8),
          )
        ),
        Card(
          shadowColor: Colors.grey.withOpacity(.4),
          elevation: 2,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(2)),
          child: Container(
            alignment: Alignment.center,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(2))
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/image/event.jpeg').paddingOnly(top: 8),
                3.height,
                Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text(
                      'STEMPreneur Day 2022',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
                6.height,
                  // Button to read details
                  Container(
                    height: 40,
                    padding: const EdgeInsets.all(5),
                    child: ElevatedButton(
                      child: const Text('Read More'),
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all(Colors.red)
                      ),
                      onPressed: () => {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => DetailEventScreen()))
                      },
                    ).cornerRadiusWithClipRRect(10),
                  ),
              ],
            ).paddingOnly(top: 8),
          )
        ),
      ])
    );
  }

  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     appBar: AppBar(
  //       title: Center(
  //         child: Text('Event NextGen',
  //               style: TextStyle(
  //                       fontSize: 24,
  //                       fontWeight: FontWeight.bold,
  //                       )),
  //       ),
  //   ),
  //   body: GridView.count(
  //     crossAxisCount: 1,
  //     padding: EdgeInsets.all(8),
  //     children: <Widget>[
  //       Container(
  //         padding: EdgeInsets.all(10),
  //         color: Colors.pink,
  //         width: 120,
  //         alignment: Alignment.topCenter,
  //       )
  //     ],
  //   ),
  // );
  // }
}